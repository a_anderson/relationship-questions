Keep Your Relationship Healthy

======================


I had this idea a while back to keep regular tabs on how my relationships are doing. The idea is to set aside times on a specific schedule so you actually talk about the important things instead of letting them slide. Here's my initial list of questions for such a conversation. Feel free to fork/mod/use at will.

======================

1. How many times a week do we have sex? 
Is it enough? Too much? Not enough? Do I enjoy it? 
Does my partner enjoy it?

2. Things that are stressing me out in my life right now are...

3. Things that make me happy in my life right now are...

4. What's the biggest thing I've been upset about? 
What's the smallest thing that upsets me but I feel is too trivial to bring up?

5. I support my partner by...
My partner supports me by...

6. A few things my partner does that he/she doesn't know I appreciate are...

7. What are my needs? Are they being met?
What are my partner's needs? Am I meeting them?

8. My favorite "us" memory is...

9. The thing I envy the most about my friends relationships is...

10. In what ways are we growing together?
In what ways are we growing apart?

11. One thing my partner doesn't know about me is...

12. In five years I see myself...
my partner...
us...

13. Our relationship is healthy, yes or no? Why?

14. Are we still in love?
I know this because...